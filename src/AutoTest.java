import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.io.*;
import java.lang.reflect.Array;
import java.sql.Driver;
import java.time.Duration;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class AutoTest {
    public static void main(String[] args) throws InterruptedException, FileNotFoundException, UnsupportedEncodingException {
        System.setProperty("webdriver.gecko.driver", "/home/eniyew/Documents/drivers/geckodriver");
        WebDriver driver = new FirefoxDriver();
        // AutoTest autoTest = new AutoTest();
        //  autoTest.portal(driver);


        driver = new FirefoxDriver();
        AutoTest autoTest=new AutoTest();
        autoTest.portal(driver);

    }
    public void portal(WebDriver driver) throws InterruptedException, FileNotFoundException, UnsupportedEncodingException {

        String appUrl = "https://portal.aait.edu.et";
        driver.get(appUrl);
        String title = driver.getTitle();
        System.out.println(title);
        driver.findElement(By.name("UserName")).sendKeys("ATR/6688/09");
        driver.findElement(By.id("Password")).sendKeys("7848");
        driver.findElement(By.className("btn-success")).click();
        String reportUrl = driver.findElement(By.id("m2")).findElement(By.className("dropdown-menu")).findElements(By.tagName("li")).get(0).findElements(By.tagName("a")).get(0).getAttribute("href");
        driver.navigate().to(reportUrl);
        String table = driver.findElement(By.className("table")).getText();
        System.out.println(table);
        PrintWriter writer = new PrintWriter("gradeReport", "UTF-8");
        writer.print(table);
        writer.close();
        Thread.sleep(5000);
        driver.quit();
        System.out.println("Finished");

    }
}
